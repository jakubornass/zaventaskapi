﻿using Microsoft.EntityFrameworkCore;
using ZavenTask.Core.Models;
using ZavenTask.Core.Persistence;

namespace ZavenTask.Infrastructure.Repositories;

public class TicketRepository : ITicketRepository
{
    private readonly ZavenTaskDbContext dbContext;

    public TicketRepository(ZavenTaskDbContext dbContext)
    {
        this.dbContext = dbContext;
    }

    public Task<Ticket?> GetById(Guid id)
    {
        return this.dbContext.Tickets
            .Include(x => x.Event)
            .FirstOrDefaultAsync(x => x.Id == id);
    }
}
