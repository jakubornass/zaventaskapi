﻿using Microsoft.EntityFrameworkCore;
using ZavenTask.Core.Models;
using ZavenTask.Core.Persistence;

namespace ZavenTask.Infrastructure.Repositories;

internal class EventRepository : IEventRepository
{
    private readonly ZavenTaskDbContext context;

    public EventRepository(ZavenTaskDbContext context)
    {
        this.context = context;
    }

    public async Task<IReadOnlyList<Event>> GetAll()
    {
        return await this.context.Events
            .Include(x => x.Tickets)
            .ToListAsync();
    }

    public void Add(Event evnt)
    {
        this.context.Events.Add(evnt);
    }

    public void Delete(Event evnt)
    {
        this.context.Events.Remove(evnt);
    }

    public async Task<Event?> GetById(Guid id)
    {
        return await this.context.Events
            .Include(x => x.Tickets)
            .FirstOrDefaultAsync(x => x.Id == id);
    }
}
