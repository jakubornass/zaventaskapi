﻿using ZavenTask.Core.Persistence;

namespace ZavenTask.Infrastructure;

internal class UnitOfWork : IUnitOfWork
{
    private readonly ZavenTaskDbContext context;

    public UnitOfWork(ZavenTaskDbContext context)
    {
        this.context = context;
    }

    public async Task SaveChanges()
    {
        await this.context.SaveChangesAsync();
    }
}
