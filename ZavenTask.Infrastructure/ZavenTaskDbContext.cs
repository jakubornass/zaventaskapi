﻿using Microsoft.EntityFrameworkCore;
using ZavenTask.Core.Models;

namespace ZavenTask.Infrastructure;

public class ZavenTaskDbContext : DbContext
{
    public DbSet<Event> Events { get; init; } = default!;
    public DbSet<Ticket> Tickets { get; init; } = default!;

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        base.OnConfiguring(optionsBuilder);
        // TODO: connection string should be taken from appsettings
        optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=ZavenTask;Trusted_Connection=True;");
    }
}
