﻿using Microsoft.Extensions.DependencyInjection;
using ZavenTask.Core.Persistence;
using ZavenTask.Infrastructure.Repositories;

namespace ZavenTask.Infrastructure.Extensions;

public static class InfrastructureServiceCollectionExtensions
{
    public static IServiceCollection AddInfrastructure(this IServiceCollection services)
    {
        services.AddDbContext<ZavenTaskDbContext>();
        services.AddTransient<IUnitOfWork, UnitOfWork>();
        services.AddTransient<IEventRepository, EventRepository>();
        services.AddTransient<ITicketRepository, TicketRepository>();

        return services;
    }
}
