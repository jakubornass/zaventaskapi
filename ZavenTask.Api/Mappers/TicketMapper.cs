﻿using ZavenTask.Api.ApiModels;
using ZavenTask.Core.Models;

namespace ZavenTask.Api.Mappers;

public static class TicketMapper
{
    public static TicketApiModel ToApiModel(this Ticket ticket)
    {
        return new TicketApiModel(
            ticket.Id,
            ticket.HolderFirstName,
            ticket.HolderLastName,
            ticket.HolderEmail,
            ticket.BookedAt,
            ticket.CancelledAt,
            ticket.IsCancelled);
    }
}
