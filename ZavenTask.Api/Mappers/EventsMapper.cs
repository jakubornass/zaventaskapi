﻿using ZavenTask.Api.ApiModels;
using ZavenTask.Core.Models;

namespace ZavenTask.Api.Mappers;

public static class EventMapper
{
    public static EventApiModel ToApiModel(this Event evnt)
    {
        return new EventApiModel(
            evnt.Id,
            evnt.Name,
            evnt.Capacity,
            evnt.CreatedAt,
            evnt.TakesPlaceAt,
            evnt.AvailableTicketsCount);
    }
}
