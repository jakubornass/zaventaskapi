﻿using Microsoft.AspNetCore.Mvc;
using ZavenTask.Api.ApiModels;
using ZavenTask.Api.Mappers;
using ZavenTask.Core.CommandHandlers;
using ZavenTask.Core.QueryHandlers;

namespace ZavenTask.Api.Controllers;

[ApiController]
[Route("events")]
public class EventsController : ControllerBase
{
    private readonly IGetEventsQueryHandler getEventsQueryHandler;
    private readonly IAddEventCommandHandler addEventHandler;
    private readonly IDeleteEventCommandHandler deleteEventCommandHandler;
    private readonly IGetEventQueryHandler getEventQueryHandler;
    private readonly ISetEventCapacityCommandHandler setEventCapacityCommandHandler;

    public EventsController(
        IGetEventsQueryHandler getEventsQueryHandler,
        IAddEventCommandHandler addEventHandler,
        IDeleteEventCommandHandler deleteEventCommandHandler,
        IGetEventQueryHandler getEventQueryHandler,
        ISetEventCapacityCommandHandler setEventCapacityCommandHandler)
    {
        this.getEventsQueryHandler = getEventsQueryHandler;
        this.addEventHandler = addEventHandler;
        this.deleteEventCommandHandler = deleteEventCommandHandler;
        this.getEventQueryHandler = getEventQueryHandler;
        this.setEventCapacityCommandHandler = setEventCapacityCommandHandler;
    }

    [HttpGet]
    public async Task<IList<EventApiModel>> GetEvents()
    {
        var events = await this.getEventsQueryHandler.GetEvents();

        return events
            .Select(x => x.ToApiModel())
            .ToList();
    }

    [HttpPost]
    public async Task AddEvent(NewEventApiModel evnt)
    {
        await this.addEventHandler.AddEvent(evnt.Name, evnt.Capacity, evnt.TakesPlaceAt);

        // TODO: consider
        //return Created(url);
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<EventApiModel>> GetEvent(Guid id)
    {
        var evnt = await this.getEventQueryHandler.GetEvent(id);

        if (evnt is null)
        {
            return NotFound();
        }

        return evnt.ToApiModel();
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult> DeleteEvent(Guid id)
    {
        var result = await this.deleteEventCommandHandler.DeleteEvent(id);

        if (result.Failed && result.Status == IDeleteEventCommandHandler.Status.NotFound)
        {
            return NotFound();
        }

        if (result.Failed)
        {
            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        return Ok();
    }

    // TODO: Consider using HTTP PATCH
    [HttpPut("{id}/capacity")]
    public async Task<ActionResult> SetEventCapacity(Guid id, SetCapacityApiModel model)
    {
        var result = await this.setEventCapacityCommandHandler.SetEventCapacity(id, model.Capacity);

        if (result.Failed && result.Status == ISetEventCapacityCommandHandler.Status.EventNotFound)
        {
            return NotFound();
        }

        if (result.Failed)
        {
            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        return Ok();
    }
}
