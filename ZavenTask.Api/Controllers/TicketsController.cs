﻿using Microsoft.AspNetCore.Mvc;
using ZavenTask.Api.ApiModels;
using ZavenTask.Api.Mappers;
using ZavenTask.Core.CommandHandlers;
using ZavenTask.Core.QueryHandlers;

namespace ZavenTask.Api.Controllers;

[ApiController]
public class TicketsController : ControllerBase
{
    private readonly IBuyTicketCommandHandler buyTicketCommandHandler;
    private readonly IGetEventTicketsQueryHandler getEventTicketsQueryHandler;
    private readonly ICancelTicketCommandHandler cancelTicketCommandHandler;

    public TicketsController(
        IBuyTicketCommandHandler buyTicketCommandHandler,
        IGetEventTicketsQueryHandler getEventTicketsQueryHandler,
        ICancelTicketCommandHandler cancelTicketCommandHandler)
    {
        this.buyTicketCommandHandler = buyTicketCommandHandler;
        this.getEventTicketsQueryHandler = getEventTicketsQueryHandler;
        this.cancelTicketCommandHandler = cancelTicketCommandHandler;
    }

    [HttpGet("events/{eventId}/tickets")]
    public async Task<IList<TicketApiModel>> GetTickets(Guid eventId)
    {
        var tickets = await this.getEventTicketsQueryHandler.GetTickets(eventId);

        return tickets
            .Select(x => x.ToApiModel())
            .ToList();
    }

    [HttpPost("events/{eventId}/tickets")]
    public async Task<ActionResult> BuyTicket(Guid eventId, BuyTicketApiModel model)
    {
        var result = await this.buyTicketCommandHandler.BuyTicket(eventId, model.FirstName, model.LastName, model.Email);

        if (result.Failed && result.Status == IBuyTicketCommandHandler.Status.EventNotFound)
        {
            return NotFound();
        }

        if (result.Failed)
        {
            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        return Ok();
    }

    [HttpPost("tickets/{ticketId}/cancel")]
    public async Task<ActionResult> CancelTicket(Guid ticketId)
    {
        var result = await this.cancelTicketCommandHandler.CancelTicket(ticketId);

        if (result.Failed)
        {
            return result.Status switch
            {
                ICancelTicketCommandHandler.Status.TicketNotFound => NotFound(),
                ICancelTicketCommandHandler.Status.TicketAlreadyCancelled => BadRequest("Ticket already cancelled"),
                _ => StatusCode(StatusCodes.Status500InternalServerError),
            };
        }

        return Ok();
    }
}
