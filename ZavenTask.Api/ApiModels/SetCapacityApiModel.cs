﻿using System.ComponentModel.DataAnnotations;

namespace ZavenTask.Api.ApiModels
{
    public record class SetCapacityApiModel
    {
        [Required]
        public int Capacity { get; init; }
    }
}
