﻿namespace ZavenTask.Api.ApiModels;

public record class TicketApiModel(
    Guid id,
    string HolderFirstName,
    string HolderLastName,
    string HolderEmail,
    DateTimeOffset BookedAt,
    DateTimeOffset? CancelledAt,
    bool IsCancelled);
