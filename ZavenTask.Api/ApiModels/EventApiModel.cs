﻿namespace ZavenTask.Api.ApiModels;

public record class EventApiModel(
    Guid Id,
    string Name,
    int Capacity,
    DateTimeOffset CreatedAt,
    DateTimeOffset TakesPlaceAt,
    int AvailableTicketsCount);
