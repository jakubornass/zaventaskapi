﻿using System.ComponentModel.DataAnnotations;

namespace ZavenTask.Api.ApiModels;

public record class NewEventApiModel
{
    [Required]
    public string Name { get; init; } = default!;

    [Required]
    public int Capacity { get; init; }

    [Required]
    public DateTimeOffset TakesPlaceAt { get; init; }
}
