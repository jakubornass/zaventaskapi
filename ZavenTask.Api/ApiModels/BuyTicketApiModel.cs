﻿using System.ComponentModel.DataAnnotations;

namespace ZavenTask.Api.ApiModels;

public record class BuyTicketApiModel
{
    [Required]
    public string FirstName { get; init; } = default!;

    [Required]
    public string LastName { get; init; } = default!;

    [Required]
    public string Email { get; init; } = default!;
}
