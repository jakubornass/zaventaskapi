using Microsoft.EntityFrameworkCore;
using ZavenTask.Api.Extensions;
using ZavenTask.Core.Extensions;
using ZavenTask.Infrastructure;
using ZavenTask.Infrastructure.Extensions;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddApi();
builder.Services.AddCore();
builder.Services.AddInfrastructure();

var app = builder.Build();


if (app.Environment.IsDevelopment())
{
    using var scope = app.Services.CreateScope();
    var dbContext = scope.ServiceProvider.GetRequiredService<ZavenTaskDbContext>();
    dbContext.Database.Migrate();
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseAuthorization();
app.MapControllers();

app.Run();
