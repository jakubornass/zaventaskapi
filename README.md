## Setup
1. cd ZavenTask/ZavenTask.Api
2. dotnet run (LocalDb should be created automatically)
3. open `/swagger`

#### dotnet-ef CLI tool setup (optional, needed only if you want to manage DB migrations)
0. install .NET Core 2.1 (e.g. via VS installer), it's required for dotnet-ef for some reason
1. cd ZavenTask
2. dotnet tool restore
3. cd ZavenTask.Infrastructure
4. dotnet-ef is ready to use: `dotnet ef --help`


## Basic info
- Application is written using ASP.NET Core 6.0 and Entity Framework Core using LocalDb as a storage layer
- It is split into 3 projects: Api, Core and Infrastructure
- Api project is just the application layer, with ASP.NET
- Core project contains the business logic, it does not depend on Api and Infrastructure projects in any way
- Infrastructure project makes use of Entity Framework and implements interfaces declared in Core
- `ZavenTaskDbContext` uses LocalDb at the time but it could easily use full SQL Server if needed
- There is a project with sample unit tests for 2 classes
- Core project uses a simplified CQRS implementation


#### Some things that have been omitted on purpose but could be improved if spent more time on it:
- full input validation in API endpoints (there are only some Required attributes used)
- full OpenAPI documentation (Swagger lists all endpoints and models properly but without full description and details)
- full unit tests coverage
- several other things which are mentioned in the code with `// TODO` comments
