﻿using ZavenTask.Core.Common;
using ZavenTask.Core.Persistence;

namespace ZavenTask.Core.CommandHandlers;

public interface ISetEventCapacityCommandHandler
{
    Task<Result<Status>> SetEventCapacity(Guid id, int capacity);

    public enum Status
    {
        EventNotFound
    }
}

public class SetEventCapacityCommandHandler : ISetEventCapacityCommandHandler
{
    private readonly IEventRepository eventRepository;
    private readonly IUnitOfWork unitOfWork;

    public SetEventCapacityCommandHandler(IEventRepository eventRepository, IUnitOfWork unitOfWork)
    {
        this.eventRepository = eventRepository;
        this.unitOfWork = unitOfWork;
    }

    public async Task<Result<ISetEventCapacityCommandHandler.Status>> SetEventCapacity(Guid id, int capacity)
    {
        var evnt = await this.eventRepository.GetById(id);

        if (evnt is null)
        {
            return Result.Fail(ISetEventCapacityCommandHandler.Status.EventNotFound);
        }

        evnt.Capacity = capacity;
        await this.unitOfWork.SaveChanges();

        return Result.Success<ISetEventCapacityCommandHandler.Status>();
    }
}
