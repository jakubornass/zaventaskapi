﻿using ZavenTask.Core.Models;
using ZavenTask.Core.Persistence;

namespace ZavenTask.Core.CommandHandlers;

public interface IAddEventCommandHandler
{
    Task AddEvent(string name, int capacity, DateTimeOffset takesPlaceAt);
}

public class AddEventCommandHandler : IAddEventCommandHandler
{
    private readonly IEventRepository eventRepository;
    private readonly IUnitOfWork unitOfWork;

    public AddEventCommandHandler(IEventRepository eventRepository, IUnitOfWork unitOfWork)
    {
        this.eventRepository = eventRepository;
        this.unitOfWork = unitOfWork;
    }

    public Task AddEvent(string name, int capacity, DateTimeOffset takesPlaceAt)
    {
        var evnt = new Event(name, capacity, DateTimeOffset.Now, takesPlaceAt);
        this.eventRepository.Add(evnt);

        return this.unitOfWork.SaveChanges();
    }
}
