﻿using ZavenTask.Core.Common;
using ZavenTask.Core.Models;
using ZavenTask.Core.Persistence;

namespace ZavenTask.Core.CommandHandlers;

public interface IBuyTicketCommandHandler
{
    Task<Result<Status>> BuyTicket(Guid eventId, string firstName, string lastName, string email);

    public enum Status
    {
        EventNotFound
    }
}

public class BuyTicketCommandHandler : IBuyTicketCommandHandler
{
    private readonly IEventRepository eventRepository;
    private readonly IUnitOfWork unitOfWork;

    public BuyTicketCommandHandler(
        IEventRepository eventRepository,
        IUnitOfWork unitOfWork)
    {
        this.eventRepository = eventRepository;
        this.unitOfWork = unitOfWork;
    }

    public async Task<Result<IBuyTicketCommandHandler.Status>> BuyTicket(Guid eventId, string firstName, string lastName, string email)
    {
        var evnt = await this.eventRepository.GetById(eventId);

        if (evnt is null)
        {
            return Result.Fail(IBuyTicketCommandHandler.Status.EventNotFound);
        }

        var ticket = new Ticket(firstName, lastName, email, evnt, DateTimeOffset.Now);
        evnt.Tickets.Add(ticket);

        await this.unitOfWork.SaveChanges();

        return Result.Success<IBuyTicketCommandHandler.Status>();
    }

}
