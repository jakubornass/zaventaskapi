﻿using ZavenTask.Core.Common;
using ZavenTask.Core.Persistence;

namespace ZavenTask.Core.CommandHandlers;

public interface IDeleteEventCommandHandler
{
    Task<Result<Status>> DeleteEvent(Guid id);

    public enum Status
    {
        NotFound
    }
}

public class DeleteEventCommandHandler : IDeleteEventCommandHandler
{
    private readonly IEventRepository eventRepository;
    private readonly IUnitOfWork unitOfWork;

    public DeleteEventCommandHandler(IEventRepository eventRepository, IUnitOfWork unitOfWork)
    {
        this.eventRepository = eventRepository;
        this.unitOfWork = unitOfWork;
    }

    public async Task<Result<IDeleteEventCommandHandler.Status>> DeleteEvent(Guid id)
    {
        var evnt = await this.eventRepository.GetById(id);

        if (evnt is null)
        {
            return Result.Fail(IDeleteEventCommandHandler.Status.NotFound);
        }

        this.eventRepository.Delete(evnt);
        await this.unitOfWork.SaveChanges();

        return Result.Success<IDeleteEventCommandHandler.Status>();
    }
}
