﻿using ZavenTask.Core.Common;
using ZavenTask.Core.Persistence;

namespace ZavenTask.Core.CommandHandlers;

public interface ICancelTicketCommandHandler
{
    Task<Result<Status>> CancelTicket(Guid ticketId);

    public enum Status
    {
        TicketNotFound,
        TicketAlreadyCancelled
    }
}

public class CancelTicketCommandHandler : ICancelTicketCommandHandler
{
    private readonly ITicketRepository ticketRepository;
    private readonly IUnitOfWork unitOfWork;

    public CancelTicketCommandHandler(ITicketRepository ticketRepository, IUnitOfWork unitOfWork)
    {
        this.ticketRepository = ticketRepository;
        this.unitOfWork = unitOfWork;
    }

    public async Task<Result<ICancelTicketCommandHandler.Status>> CancelTicket(Guid ticketId)
    {
        var ticket = await this.ticketRepository.GetById(ticketId);

        if (ticket is null)
        {
            return Result.Fail(ICancelTicketCommandHandler.Status.TicketNotFound);
        }

        if (ticket.IsCancelled)
        {
            return Result.Fail(ICancelTicketCommandHandler.Status.TicketAlreadyCancelled);
        }

        ticket.Cancel();
        await this.unitOfWork.SaveChanges();

        return Result.Success<ICancelTicketCommandHandler.Status>();
    }
}
