﻿namespace ZavenTask.Core.Models;

public class Event
{
    public Guid Id { get; init; }
    public string Name { get; init; }
    public int Capacity { get; set; }
    public DateTimeOffset CreatedAt { get; init; }
    public DateTimeOffset TakesPlaceAt { get; init; }

    public ICollection<Ticket> Tickets { get; init; } = new List<Ticket>();

    public int SoldTicketsCount => Tickets.Count(x => !x.IsCancelled);
    public int AvailableTicketsCount => Capacity - SoldTicketsCount;

    public Event(string name, int capacity, DateTimeOffset createdAt, DateTimeOffset takesPlaceAt)
    {
        Name = name;
        Capacity = capacity;
        CreatedAt = createdAt;
        TakesPlaceAt = takesPlaceAt;
    }
}
