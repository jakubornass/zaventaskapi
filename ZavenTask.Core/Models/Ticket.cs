﻿namespace ZavenTask.Core.Models;

public class Ticket
{
    public Guid Id { get; init; }
    
    // TODO: Consider separate table or inner class
    public string HolderFirstName { get; init; }
    public string HolderLastName { get; init; }
    public string HolderEmail { get; init; }
    public DateTimeOffset BookedAt { get; init; }
    public DateTimeOffset? CancelledAt { get; set; }
    public bool IsCancelled => CancelledAt is not null;

    public Event Event { get; init; }


    public Ticket(
        string holderFirstName,
        string holderLastName,
        string holderEmail,
        Event evnt,
        DateTimeOffset bookedAt)
    {
        HolderFirstName = holderFirstName;
        HolderLastName = holderLastName;
        HolderEmail = holderEmail;
        Event = evnt;
        BookedAt = bookedAt;
    }

    private Ticket()
    {
        HolderFirstName = default!;
        HolderLastName = default!;
        HolderEmail = default!;
        Event = default!;
    }

    public void Cancel()
    {
        CancelledAt = DateTimeOffset.Now;
    }
}
