﻿namespace ZavenTask.Core.Common;

public class Result
{
    public bool Succeeded { get; }
    public bool Failed => !Succeeded;

    protected Result(bool succeeded)
    {
        Succeeded = succeeded;
    }

    public static Result Success() => new(succeeded: true);

    public static Result Fail() => new(succeeded: false);

    public static Result<TStatus> Success<TStatus>()
        where TStatus : struct, Enum
    {
        return new Result<TStatus>(succeeded: true);
    }

    public static Result<TStatus> Fail<TStatus>(TStatus status)
        where TStatus : Enum
    {
        return new Result<TStatus>(succeeded: false, status);
    }
}

public class Result<TStatus> : Result
    where TStatus : Enum
{
    public TStatus? Status { get; }

    internal Result(bool succeeded, TStatus? status = default)
        : base(succeeded)
    {
        Status = status;
    }
}
