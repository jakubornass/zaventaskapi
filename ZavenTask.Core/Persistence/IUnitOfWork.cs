﻿namespace ZavenTask.Core.Persistence;

public interface IUnitOfWork
{
    Task SaveChanges();
}
