﻿using ZavenTask.Core.Models;

namespace ZavenTask.Core.Persistence;

public interface IEventRepository
{
    Task<IReadOnlyList<Event>> GetAll();
    void Add(Event evnt);
    void Delete(Event evnt);
    Task<Event?> GetById(Guid id);
}
