﻿using ZavenTask.Core.Models;

namespace ZavenTask.Core.Persistence;

public interface ITicketRepository
{
    Task<Ticket?> GetById(Guid id);
}
