﻿using Microsoft.Extensions.DependencyInjection;
using ZavenTask.Core.CommandHandlers;
using ZavenTask.Core.QueryHandlers;

namespace ZavenTask.Core.Extensions;

public static class CoreServiceCollectionExtensions
{
    public static IServiceCollection AddCore(this IServiceCollection services)
    {
        services.AddTransient<IGetEventsQueryHandler, GetEventsQueryHandler>();
        services.AddTransient<IAddEventCommandHandler, AddEventCommandHandler>();
        services.AddTransient<IDeleteEventCommandHandler, DeleteEventCommandHandler>();
        services.AddTransient<IBuyTicketCommandHandler, BuyTicketCommandHandler>();
        services.AddTransient<ICancelTicketCommandHandler, CancelTicketCommandHandler>();
        services.AddTransient<IGetEventQueryHandler, GetEventQueryHandler>();
        services.AddTransient<IGetEventTicketsQueryHandler, GetEventTicketsQueryHandler>();
        services.AddTransient<ISetEventCapacityCommandHandler, SetEventCapacityCommandHandler>();

        return services;
    }
}
