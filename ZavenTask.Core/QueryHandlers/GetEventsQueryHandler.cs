﻿using ZavenTask.Core.Models;
using ZavenTask.Core.Persistence;

namespace ZavenTask.Core.QueryHandlers;

public interface IGetEventsQueryHandler
{
    Task<IReadOnlyList<Event>> GetEvents();
}

public class GetEventsQueryHandler : IGetEventsQueryHandler
{
    private readonly IEventRepository eventRepository;

    public GetEventsQueryHandler(IEventRepository eventRepository)
    {
        this.eventRepository = eventRepository;
    }

    public async Task<IReadOnlyList<Event>> GetEvents()
    {
        return await this.eventRepository.GetAll();
    }
}
