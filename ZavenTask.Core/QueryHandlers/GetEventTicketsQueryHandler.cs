﻿using ZavenTask.Core.Models;
using ZavenTask.Core.Persistence;

namespace ZavenTask.Core.QueryHandlers;

public interface IGetEventTicketsQueryHandler
{
    Task<IReadOnlyList<Ticket>> GetTickets(Guid eventId);
}

internal class GetEventTicketsQueryHandler : IGetEventTicketsQueryHandler
{
    private readonly IEventRepository eventRepository;

    public GetEventTicketsQueryHandler(IEventRepository eventRepository)
    {
        this.eventRepository = eventRepository;
    }

    public async Task<IReadOnlyList<Ticket>> GetTickets(Guid eventId)
    {
        var evnt = await this.eventRepository.GetById(eventId);

        // TODO: Use Result with status instead of throwing exception
        if (evnt is null)
        {
            throw new Exception("Event not found");
        }

        return evnt.Tickets.ToList();
    }
}
