﻿using ZavenTask.Core.Models;
using ZavenTask.Core.Persistence;

namespace ZavenTask.Core.QueryHandlers;

public interface IGetEventQueryHandler
{
    Task<Event?> GetEvent(Guid eventId);
}

public class GetEventQueryHandler : IGetEventQueryHandler
{
    private readonly IEventRepository eventRepository;

    public GetEventQueryHandler(IEventRepository eventRepository)
    {
        this.eventRepository = eventRepository;
    }

    public Task<Event?> GetEvent(Guid eventId)
    {
        return this.eventRepository.GetById(eventId);
    }
}
