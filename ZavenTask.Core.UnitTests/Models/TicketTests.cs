﻿using FluentAssertions;
using System;
using Xunit;
using ZavenTask.Core.Models;

namespace ZavenTask.Core.UnitTests.Models;

public class TicketTests
{
    [Fact]
    public void Constructor_CancelledAtIsNull()
    {
        var ticket = GetExampleTicket();

        ticket.CancelledAt.Should().BeNull();
    }

    [Fact]
    public void Constructor_TicketIsNotCancelled()
    {
        var ticket = GetExampleTicket();

        ticket.IsCancelled.Should().BeFalse();
    }

    [Fact]
    public void Cancel_IsCancelled()
    {
        var ticket = GetExampleTicket();

        ticket.Cancel();

        ticket.IsCancelled.Should().BeTrue();
    }

    [Fact]
    public void Cancel_CancelledAtIsCurrentDateTime()
    {
        var ticket = GetExampleTicket();

        ticket.Cancel();

        ticket.CancelledAt.Should().BeCloseTo(DateTimeOffset.Now, TimeSpan.FromSeconds(1));
    }

    private static Ticket GetExampleTicket()
    {
        var evnt = new Event(string.Empty, default, default, default);

        return new Ticket(string.Empty, string.Empty, string.Empty, evnt, default);
    }
}
