﻿using FluentAssertions;
using NSubstitute;
using System;
using System.Threading.Tasks;
using Xunit;
using ZavenTask.Core.CommandHandlers;
using ZavenTask.Core.Common;
using ZavenTask.Core.Models;
using ZavenTask.Core.Persistence;

namespace ZavenTask.Core.UnitTests.CommandHandlers;

public class SetEventCapacityCommandHandlerTests
{
    private readonly SetEventCapacityCommandHandler handler;

    private readonly IEventRepository eventRepository = Substitute.For<IEventRepository>();
    private readonly IUnitOfWork unitOfWork = Substitute.For<IUnitOfWork>();

    public SetEventCapacityCommandHandlerTests()
    {
        this.handler = new SetEventCapacityCommandHandler(this.eventRepository, this.unitOfWork);
    }

    [Fact]
    public async Task SetEventCapacity_EventNotFound_Failed()
    {
        var result = await this.handler.SetEventCapacity(Guid.NewGuid(), 100);

        var expectedResult = Result.Fail(ISetEventCapacityCommandHandler.Status.EventNotFound);
        result.Should().BeEquivalentTo(expectedResult);
    }

    [Fact]
    public async Task SetEventCapacity_ValidCapacity_PropertyUpdated()
    {
        var eventId = Guid.NewGuid();
        var evnt = new Event(string.Empty, default, default, default);
        eventRepository.GetById(eventId)
            .Returns(evnt);

        const int newCapacity = 100;
        await this.handler.SetEventCapacity(eventId, newCapacity);

        evnt.Capacity.Should().Be(newCapacity);
    }

    [Fact]
    public async Task SetEventCapacity_ValidCapacity_Succeeded()
    {
        var eventId = Guid.NewGuid();
        var evnt = new Event(string.Empty, default, default, default);
        eventRepository.GetById(eventId)
            .Returns(evnt);

        var result = await this.handler.SetEventCapacity(eventId, 100);

        var expectedResult = Result.Success<ISetEventCapacityCommandHandler.Status>();
        result.Should().BeEquivalentTo(expectedResult);
    }

    [Fact]
    public async Task SetEventCapacity_ValidCapacity_SavedChanges()
    {
        var eventId = Guid.NewGuid();
        var evnt = new Event(string.Empty, default, default, default);
        eventRepository.GetById(eventId)
            .Returns(evnt);

        await this.handler.SetEventCapacity(eventId, 100);

        await this.unitOfWork.Received().SaveChanges();
    }
}
